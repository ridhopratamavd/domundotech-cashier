const express = require('express');
const router = express.Router();

const { get } = require('../controllers/absent');

// Route get semua product
router.get('/:name', get);

module.exports = router;
