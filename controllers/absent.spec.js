const chai = require('chai');
const { stub, resetHistory } = require('sinon');
const proxyquire = require('proxyquire');
const sinonChai = require('sinon-chai');

const chaiHttp = require('chai-http');
const app = require('../App');
const { makeMockModels } = require('sequelize-test-helpers');
const should = chai.should();
const moment = require('moment-timezone');
moment().tz('Asia/Jakarta').format();
const mockDate = require('mockdate');
const { mockRequest } = require('mock-req-res');

chai.use(sinonChai);
chai.use(chaiHttp);

describe('controllers/absents', () => {
  const absents = { findOrCreate: stub() };
  const mockModels = makeMockModels({ absents });

  const { get } = proxyquire('./absent', {
    '../models': mockModels
  });

  const requestForHTTPCall = mockRequest({
    'user-agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_5_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.2 Mobile/15E148 Safari/604.1'
  });

  const requestForFunctionCall = {
    headers: {
      'user-agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_5_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.2 Mobile/15E148 Safari/604.1'
    },
    params: { name: 'rio' }
  };

  const res = {
    send: function () {},
    json: function (d) {
      console.log('\n : ' + d); ;
    },
    status: function (s) {
      this.statusCode = s;
      return this;
    }
  };

  context('context #1 return 201', () => {
    before(async () => {
      mockDate.set(moment('03-29-2021', 'MM-DD-YYYY').toDate()); // senin
      absents.findOrCreate.resolves([{}, true]);
    });

    after(() => {
      resetHistory();
      mockDate.reset();
    });

    it('should return created status when success create record', (done) => {
      chai.request(app)
        .get('/absent/rio')
        .set(requestForHTTPCall)
        .end((_, res) => {
          res.should.have.status(201);
          done();
        });
    });

    it('should call findOrCreate when receive request from controller', async () => {
      await get(requestForFunctionCall, res);
      chai.expect(absents.findOrCreate).to.have.been.called;
    });
  });

  context('context #2 return 409', () => {
    before(async () => {
      mockDate.set(moment('03-29-2021', 'MM-DD-YYYY').toDate()); // senin
      absents.findOrCreate.resolves([{}, false]);
    });

    after(() => {
      resetHistory();
      mockDate.reset();
    });

    it('should return 409 when findOrCreate return false due to exist for absents monday', (done) => {
      chai.request(app)
        .get('/absent/rio')
        .set(requestForHTTPCall)
        .end((_, res) => {
          res.should.have.status(409);
          done();
        });
    });
  });

  context('context #3 return 401', () => {
    before(async () => {
      mockDate.set(moment('03-28-2021', 'MM-DD-YYYY')); // sunday
      absents.findOrCreate.resolves([{}, false]);
    });

    after(resetHistory);

    it('should return 401 when day is sunday', async () => {
      await get(requestForFunctionCall, res);
      chai.expect(absents.findOrCreate).to.not.have.been.called;
    });
  });

  context('context #4 return 401', () => {
    before(async () => {
      mockDate.set(moment('03-29-2021', 'MM-DD-YYYY')); // monday
      absents.findOrCreate.resolves([{}, false]);
    });

    after(resetHistory);

    it('should return 401 when is not valid device', (done) => {
      const requestForHTTPCall = mockRequest({
        'user-agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 14_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14_0 Mobile/15E148 Safari/604.1'
      });
      chai.request(app)
        .get('/absent/rio')
        .set(requestForHTTPCall)
        .end((_, res) => {
          res.should.have.status(401);
          done();
        });
    });
  });

  context('context #5 return 401', () => {
    before(async () => {
      mockDate.set(moment('03-29-2021', 'MM-DD-YYYY')); // monday
      absents.findOrCreate.resolves([{}, false]);
    });

    after(resetHistory);

    it('should return 401 when is not valid user', (done) => {
      chai.request(app)
        .get('/absent/ooredoo')
        .set(requestForHTTPCall)
        .end((_, res) => {
          res.should.have.status(401);
          done();
        });
    });
  });

  // context('context #3', () => {
  //   before((done) => {
  //     absents.findOrCreate.resolves([{}, true]);
  //     done();
  //   });

  //   after(resetHistory);

  //   it('called absents.findOrCreate', async () => {
  //     const result = await createRecord('rio');
  //     // chai.expect(absents.findOrCreate).to.have.been.called;
  //     chai.expect(result[1]).to.be.true;
  //   });
  // });

  // context('context #4', () => {
  //   before((done) => {
  //     absents.findOrCreate.resolves([{}, false]);
  //     done();
  //   });

  //   after(resetHistory);

  //   it('called absents.findOrCreate', async () => {
  //     const result = await createRecord('rio');
  //     // chai.expect(absents.findOrCreate).to.have.been.called;
  //     chai.expect(result[1]).to.be.false;
  //   });
  // });
});
