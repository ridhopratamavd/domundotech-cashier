const DeviceDetector = require('node-device-detector');
const moment = require('moment-timezone');
moment().tz('Asia/Jakarta').format();
const { INVALID_REQUEST, CREATED, RECORD_ALREADY_CREATED } = require('../constants/response');

const deviceDetector = new DeviceDetector();

// database
const { Sequelize } = require('sequelize');
const { absents } = require('../models');

exports.createAbsent = async (req, res) => {
  // try {
  //   const useragent = req.headers['user-agent'];
  //   req.useragent = useragent;
  //   req.device = deviceDetector.detect(useragent);
  //   console.log(req);
  //   await Absents.create(req.body);
  //   res.json({
  //     message: 'Absent Created'
  //   });
  // } catch (err) {
  //   console.log(err);
  // }
};
const get = async (req, res) => {
  const userAgent = deviceDetector.detect(req.headers['user-agent']);
  if (isValidDayAndUserAgent(userAgent)) {
    const name = req.params.name;
    const privilegedMember = ['rio', 'alam', 'aldi', 'romi', 'ridho'];
    if (privilegedMember.includes(name)) {
      const result = await createRecord(name);
      if (result[1] === true) {
        res.status(201).json(CREATED);
      } else {
        res.status(409).json(RECORD_ALREADY_CREATED);
      }
    } else {
      res.status(401).json(INVALID_REQUEST);
    }
  } else {
    res.status(401).json(INVALID_REQUEST);
  }
};

function isValidDayAndUserAgent (request) {
  const isNotWeekend = moment(new Date()).day() !== 0;
  if (request.os.version === '12.5.1' &&
          request.client.name === 'Mobile Safari' &&
          request.client.version === '12.1.2' &&
          isNotWeekend) {
    return true;
  } else {
    return false;
  }
}

const createRecord = (name) => {
  const Op = Sequelize.Op;
  const NOW_DATE = moment(new Date()).format('YYYY-MM-DD');
  const NOW_TIME = moment(new Date()).format('HH:mm:ss');
  return absents.findOrCreate({
    where: {
      name: name,
      date: {
        [Op.eq]: NOW_DATE
      }
    },
    defaults: {
      name,
      date: NOW_DATE,
      time: NOW_TIME
    }
  });
};

module.exports = { get, createRecord };
