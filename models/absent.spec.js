const {
  sequelize,
  dataTypes,
  checkModelName,
  checkPropertyExists
} = require('sequelize-test-helpers');
const absents = require('./absent');
describe('../absents', () => {
  const Model = absents(sequelize, dataTypes);
  const instance = new Model();
  checkModelName(Model)('absents');
  context('properties', () => {
    ;['name', 'date', 'time'].forEach(checkPropertyExists(instance));
  });
});
