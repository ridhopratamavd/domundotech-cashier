exports.RECORD_ALREADY_CREATED = {
  message: 'record already created'
};

exports.CREATED = {
  message: 'record created'
};

exports.INVALID_REQUEST = {
  message: 'invalid request'
};
